
/**
  * Case class that defines a Station.
  * @param name name of the station.
  */
case class Station(name: String)