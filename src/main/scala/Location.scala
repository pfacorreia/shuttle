
/**
  * Enumerate that defines the different statuses for the Shuttle.
  *
  * startStation - Location that defines that the shuttle is currently at the startStation or moving away from it towards the endStation.
  * endStation - Location that defines that the shuttle is currently at the endStation or moving away from it towards the startStation.
  */
object Location extends Enumeration {

  type Location = String

  lazy val startStation = "StartStation"
  lazy val endStation = "EndStation"
}
