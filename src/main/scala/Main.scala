
/**
  * Object that contains the main method.
  */
object Main extends App{

  new ShuttleSystem(args(0).toLong, args(1).toLong, args(2).toLong, args(3).toLong, args(4).toLong, args(5).toLong, args(6).toDouble).start()

}

/*def exercise = { //exercise that was asked in the readme
    val AStation = Station("A")
    val BStation = Station("B")
    val path = Path(AStation, BStation, Duration.ofMinutes(9), Duration.ofMinutes(11))
    messageFeed
    shuttle = new Shuttle("DefaultShuttle", path, Duration.ofMinutes(7).plusSeconds(30), Duration.ofHours(8), Duration.ofMinutes(5).plusSeconds(30)).start
  }

  def exerciseDownscaled = { //exercise that was asked in the readme but downscaled for better testing (hours are now minutes and minutes are seconds)
    val AStation = Station("A")
    val BStation = Station("B")
    val path = Path(AStation, BStation, Duration.ofSeconds(9), Duration.ofSeconds(11))
    messageFeed
    shuttle = new Shuttle("DefaultShuttle", path, Duration.ofSeconds(7).plusMillis(500), Duration.ofMinutes(8), Duration.ofSeconds(5).plusMillis(500)).start
  }

  def test = { //exercise that I used for tests myself
    val AStation = Station("A")
    val BStation = Station("B")
    val path = Path(AStation, BStation, Duration.ofSeconds(10), Duration.ofSeconds(20))
    messageFeed
    shuttle = new Shuttle("DefaultShuttle", path, Duration.ofSeconds(10), Duration.ofSeconds(30), Duration.ofSeconds(5)).start
  }*/