
/**
  * Object that holds methods regarding the terminal notifications for the shuttle's actions.
  */
object NotificationSystem {

  /**
    * Method that writes a notification to the standard output when the shuttle departs a station.
    * @param shuttle The shuttle instance.
    */
  def departureMessage(shuttle: Shuttle): Unit = {
    if(shuttle.location == Location.startStation)
      println("(" + shuttle.name + "): " + "Shuttle has departed towards " + shuttle.path.endStation + ".")
    else
      println("(" + shuttle.name + "): " + "Shuttle has departed towards " + shuttle.path.startStation + ".")
  }

  /**
    * Method that writes a notification to the standard output when the shuttle arrives at a station.
    * @param shuttle The shuttle instance.
    */
  def arrivalMessage(shuttle: Shuttle): Unit = {
    if(shuttle.location == Location.startStation)
      println("(" + shuttle.name + "): " + "Shuttle has arrived in " + shuttle.path.endStation + ".")
    else
      println("(" + shuttle.name + "): " + "Shuttle has arrived in " + shuttle.path.startStation + ".")
  }

  /**
    * Method that writes a notification to the standard output when the shuttle driver's shift ended.
    * @param shuttle The shuttle instance.
    */
  def shiftTimeoutMessage(shuttle: Shuttle): Unit = {
    println("(" + shuttle.name + "): " + "Shuttle driver's shift ended and will switch driver in the next stop.")
  }

  /**
    * Method that writes a notification to the standard output when the shuttle's driver is changing.
    * @param shuttle
    */
  def changingShiftsMessage(shuttle: Shuttle): Unit = {
    println("(" + shuttle.name + "): " + "Shuttle is changing drivers.")
  }

  /**
    * Method that writes a notification to the standard output when the shuttle's driver has changed and the shuttle is ready to go.
    * @param shuttle
    */
  def shiftChangeCompleteMessage(shuttle: Shuttle): Unit = {
    println("(" + shuttle.name + "): " + "Shuttle has switched driver.")
  }

}
