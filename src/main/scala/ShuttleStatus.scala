
/**
  * Enumerate that defines the different statuses for the Shuttle.
  *
  * Idle - Shuttle was initialized and didn't start yet.
  * InTransit - Shuttle is currently moving from one station to another.
  * Waiting - Shuttle is currently waiting after reaching its destination.
  * ChangingShifts - Shuttle is currently changing drivers.
  */
object ShuttleStatus extends Enumeration {

  type ShuttleStatus = String

  lazy val Idle = "Idle"
  lazy val InTransit = "InTransit"
  lazy val Waiting = "Waiting"
  lazy val ChangingShifts = "ChangingShifts"
}
