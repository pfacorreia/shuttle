import scala.concurrent.duration.FiniteDuration

/**
  * Case class that holds all the information about the message feed.
  * @param messageDuration FiniteDuration that represents the interval between each message that is sent to the driver.
  * @param probUrgent Double between 0 and 1 that defines the probability for a message to be urgent.
  */
case class MessageFeed(messageDuration: FiniteDuration, probUrgent: Double) {

  def chanceOfUrgent: Boolean = math.random < probUrgent
}


