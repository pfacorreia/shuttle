import akka.actor.{Actor, ActorLogging, Timers}
import Location.Location
import NotificationSystem._
import ShuttleStatus.ShuttleStatus
import Shuttle._

import scala.collection.mutable.Queue
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.ExecutionContext

/**
  * Companion object of the Shuttle class. It contains the different case objects that can be sent to the Shuttle actor as messages.
  */
object Shuttle {

  case object Start                                                     // starts the shuttle
  case object Move                                                      // puts the shuttle in transit
  case object Wait                                                      // puts the shuttle waiting
  case object ShiftTimeout                                              // signals that the shift has ended
  case object ChangeShift                                               // switches shifts on the shuttle
  case class Message(sender: String, content: String, urgent: Boolean)  // sends message

  case object GetStatus                                                 // checks the shuttle status
  case object GetLocation                                               // checks the shuttle location
  case object ShiftEnded                                                // checks if the shuttle shift has ended or not

}

/**
  * Class that defines the Shuttle. The shuttle is also an actor being able to receive messages and operate the train with its methods.
  * @param name name of the shuttle.
  * @param path path class which contains the tripDuration and inverseTripDuration for the shuttle.
  * @param messageFeed messageFeed class which contains all the info related to the messages that are sent to the shuttle.
  * @param waitDuration FiniteDuration representing how long the shuttle has to wait after departing from its current station.
  * @param shiftDuration FiniteDuration representing how long a shift is for each driver.
  * @param shiftChangeDuration FiniteDuration representing how long it takes for drivers to switch when the shift ends.
  */
class Shuttle(val name: String,
              val path: Path,
              val messageFeed: MessageFeed,
              waitDuration: FiniteDuration,
              shiftDuration: FiniteDuration,
              shiftChangeDuration: FiniteDuration) extends Actor with Timers with ActorLogging{

  implicit val ex: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  // These vars wouldn't be needed if a database (or another way to maintain status) was implemented.
  var status: ShuttleStatus = ShuttleStatus.Idle
  var location: Location = Location.startStation
  var shiftEnded = false

  val messageList: Queue[Message] = new Queue[Message]

  /**
    * Actor method that defines how to act when it receives a message. It acts differently depending on the message
    * @return
    */
  def receive: Receive = {
    case Start => start
    case Move =>
      arrivalMessage(Shuttle.this)
      arrive()
    case Wait =>
      if (shiftEnded){
        changingShiftsMessage(Shuttle.this)
        changeShifts()
      }
      else{
        departureMessage(Shuttle.this)
        depart
      }
    case ShiftTimeout =>
      shiftTimeoutMessage(Shuttle.this)
      shiftEnded = true
    case ChangeShift =>
      shiftChangeCompleteMessage(Shuttle.this)
      departAfterShiftChange()
    case message: Message =>
      if(status == ShuttleStatus.Waiting || message.urgent) readMessage(message)
      else queueMessage(message)
      scheduleMessage()

    //actor getters
    case GetStatus => sender() ! status
    case GetLocation => sender() ! location
    case ShiftEnded => sender() ! shiftEnded

    // actor setters
    case newStatus: ShuttleStatus => status = newStatus
    case newLocation: Location => location = newLocation
    case newShift: Boolean => shiftEnded = newShift
  }

  /**
    * Method that starts the shuttle when it is initialized.
    */
  def start: Unit = {
    timers.startSingleTimer("shiftKey", ShiftTimeout, shiftDuration)
    departureMessage(this)
    scheduleMessage()
    depart
  }

  /**
    * Method that runs when the shuttle departs from the station it was currently at.
    */
  def depart: Unit = {
    status = ShuttleStatus.InTransit
    if (location == Location.startStation) timers.startSingleTimer("departKey", Move, path.tripDuration)
    else timers.startSingleTimer("departKey", Move, path.inverseTripDuration)
  }

  /**
    * Method that runs when the shuttle departs from the station it was currently at after a shift change.
    */
  def departAfterShiftChange(): Unit = {
    shiftEnded = false
    timers.startSingleTimer("shiftKey", ShiftTimeout, shiftDuration)
    departureMessage(this)
    depart
  }

  /**
    * Method that runs when the shuttle arrives at its destination.
    */
  def arrive(): Unit = {
    status = ShuttleStatus.Waiting
    checkMessages()
    if (location == Location.startStation) location = Location.endStation
    else if(location == Location.endStation) location = Location.startStation
    timers.startSingleTimer("waitKey", Wait, waitDuration)

  }

  /**
    * Method that runs when the shuttle has to change drivers.
    */
  def changeShifts(): Unit = {
    status = ShuttleStatus.ChangingShifts
    timers.startSingleTimer("changeShiftsKey", ChangeShift, shiftChangeDuration)
  }

  //MESSAGES

  /**
    * Method that runs when a new message is scheduled to be sent to the driver of the shuttle.
    */
  def scheduleMessage(): Unit = {
    timers.startSingleTimer("messageKey", Message("Person", "\"message\"", messageFeed.chanceOfUrgent), messageFeed.messageDuration)
  }

  /**
    * Method that runs when the shuttle's driver is going to read a message
    * @param message message to be read by the shuttle driver.
    */
  def readMessage(message: Message): Unit = {
    if(message.urgent) println("(URGENT MESSAGE) To[" + name + "] From[" + message.sender + "]: " + message.content)
    else println("(Regular Message) To[" + name + "] From[" + message.sender + "]: " + message.content)
  }

  /**
    * Method that queues a message when it can't be read immediately
    * @param message
    */
  def queueMessage(message: Message): Unit = messageList += message

  /**
    * Method that checks all the messages on the queue and reads them.
    */
  def checkMessages(): Unit = messageList.dequeueAll(_ => true).foreach(readMessage)

}

