import java.util.concurrent.TimeUnit
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import Shuttle._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.concurrent.duration.FiniteDuration

/**
  * Starts the shuttle simulation.
  * @param tripDuration shuttle trip duration. (in milliseconds)
  * @param inverseTripDuration shuttle trip duration when going the opposite direction. (in milliseconds)
  * @param waitDuration shuttle waiting duration at stations. (in milliseconds)
  * @param shiftDuration duration of a shift. (in milliseconds)
  * @param shiftChangeDuration time it takes for the shuttle to change shifts. (in milliseconds)
  * @param messageInterval duration between each message sent to the driver. (in milliseconds)
  * @param probUrgent probability of a message being urgent. (between 0 and 1)
  */
class ShuttleSystem(
                     tripDuration: Long,
                     inverseTripDuration: Long,
                     waitDuration: Long,
                     shiftDuration: Long,
                     shiftChangeDuration: Long,
                     messageInterval: Long,
                     probUrgent: Double
){

  val AStation = Station("A")
  val BStation = Station("B")
  val path = Path(AStation, BStation, FiniteDuration.apply(tripDuration, TimeUnit.MILLISECONDS), FiniteDuration.apply(inverseTripDuration, TimeUnit.MILLISECONDS)) //creates path for the shuttle
  val messageFeed = MessageFeed(FiniteDuration.apply(messageInterval, TimeUnit.MILLISECONDS), probUrgent) //message feed info

  implicit val ex: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  val system = ActorSystem("ShuttleSystem")
  val shuttleRef: ActorRef = system.actorOf(Props(classOf[Shuttle], "DefaultShuttle", path, messageFeed, FiniteDuration.apply(waitDuration, TimeUnit.MILLISECONDS), FiniteDuration.apply(shiftDuration, TimeUnit.MILLISECONDS), FiniteDuration.apply(shiftChangeDuration, TimeUnit.MILLISECONDS)), "ShuttleActor")
  implicit val sd: ExecutionContextExecutor = system.dispatcher

  /**
    * Starts the shuttle system by sending a Start message to the shuttle actor.
    */
  def start(): Unit = {
    shuttleRef ! Start
  }


}
