import scala.concurrent.duration.FiniteDuration

/**
  * Case class that holds the information about the shuttle's path.
  * @param startStation Station at the start of the path
  * @param endStation Station at the end of the path
  * @param tripDuration FiniteDuration that represents how long it takes to go from the starting station to the ending station.
  * @param inverseTripDuration FiniteDuration that represents how long it takes to go from the ending station to the starting station.
  */
case class Path(startStation: Station, endStation: Station, tripDuration: FiniteDuration, inverseTripDuration: FiniteDuration)

