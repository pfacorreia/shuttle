import java.io.ByteArrayOutputStream
import java.util.concurrent.TimeUnit

import Shuttle._
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration.FiniteDuration

class ActorSuite extends TestKit(ActorSystem("TestSystem")) with ImplicitSender with WordSpecLike with BeforeAndAfterAll with Matchers{

  val AStation = Station("A")
  val BStation = Station("B")

  override def afterAll: Unit = TestKit.shutdownActorSystem(system)

  "ShuttleActor#Move" should {
    "change the shuttle status to Waiting and the location to the endStation." in {

      val path = Path(AStation, BStation, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS))
      val messageFeed = MessageFeed(FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), 0)
      val shuttleRef: ActorRef = system.actorOf(Props(classOf[Shuttle], "TestShuttle1", path, messageFeed, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS)), "ShuttleActor1")

      shuttleRef ! Move
      shuttleRef ! GetStatus
      expectMsg(ShuttleStatus.Waiting)
      shuttleRef ! GetLocation
      expectMsg(Location.endStation)
      system.stop(shuttleRef)

    }
  }

  "ShuttleActor#Wait (shiftEnded == false)" should {
    "change the status to InTransit." in {

      val path = Path(AStation, BStation, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS))
      val messageFeed = MessageFeed(FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), 0)
      val shuttleRef: ActorRef = system.actorOf(Props(classOf[Shuttle], "TestShuttle3", path, messageFeed, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS)), "ShuttleActor3")

      shuttleRef ! Wait
      shuttleRef ! GetStatus
      expectMsg(ShuttleStatus.InTransit)
      system.stop(shuttleRef)
    }
  }

  "ShuttleActor#Wait (shiftEnded == true)" should {
    "change the status to ChangingShifts." in {

      val path = Path(AStation, BStation, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS))
      val messageFeed = MessageFeed(FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), 0)
      val shuttleRef: ActorRef = system.actorOf(Props(classOf[Shuttle], "TestShuttle4", path, messageFeed, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS)), "ShuttleActor4")

      shuttleRef ! true
      shuttleRef ! Wait
      shuttleRef ! GetStatus
      expectMsg(ShuttleStatus.ChangingShifts)
      system.stop(shuttleRef)
    }
  }

  "ShuttleActor#ShiftTimeout" should {
    "change the shift status to true (shift ended)." in {

      val path = Path(AStation, BStation, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS))
      val messageFeed = MessageFeed(FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), 0)
      val shuttleRef: ActorRef = system.actorOf(Props(classOf[Shuttle], "TestShuttle5", path, messageFeed, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS)), "ShuttleActor5")

      shuttleRef ! ShiftTimeout
      shuttleRef ! ShiftEnded
      expectMsg(true)
      system.stop(shuttleRef)
    }
  }


  "ShuttleActor#ChangeShift" should {
    "change the shift status to false (shift has not ended)." in {

      val path = Path(AStation, BStation, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS))
      val messageFeed = MessageFeed(FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), 0)
      val shuttleRef: ActorRef = system.actorOf(Props(classOf[Shuttle], "TestShuttle6", path, messageFeed, FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS), FiniteDuration.apply(10000, TimeUnit.MILLISECONDS)), "ShuttleActor6")

      shuttleRef ! true
      shuttleRef ! ChangeShift
      shuttleRef ! ShiftEnded
      expectMsg(false)
      system.stop(shuttleRef)
    }
  }

}
