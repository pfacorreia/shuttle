import java.io.ByteArrayOutputStream

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest._

class FunctionalSuite extends TestKit(ActorSystem("TestSystem")) with ImplicitSender with WordSpecLike with BeforeAndAfterAll with Matchers{

  val AStation = Station("A")
  val BStation = Station("B")

  """Simulation #1:
     - tripDuration: 1 second. (1000 millis)
     - inverseTripDuration: 2 seconds. (2000 millis)
     - waitDuration: 1 second. (1000 millis)
     - shiftDuration: 3 seconds. (3000 millis)
     - shiftChangeDuration: 0,5 seconds. (500 millis)
     - messageInterval: 1,2 seconds. (1200 millis)
     - probUrgent: 0% (0)
  """ should {
    "print out a specific order of notifications after 6 seconds" in {
      val stream = new ByteArrayOutputStream
      Console.withOut(stream) {
        new ShuttleSystem(1000, 2000, 1000, 3000, 500, 1200, 0).start
        Thread.sleep(6000)
      }
      assert(stream.toString ==
        """(DefaultShuttle): Shuttle has departed towards Station(B).
          |(DefaultShuttle): Shuttle has arrived in Station(B).
          |(Regular Message) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle has departed towards Station(A).
          |(DefaultShuttle): Shuttle driver's shift ended and will switch driver in the next stop.
          |(DefaultShuttle): Shuttle has arrived in Station(A).
          |(Regular Message) To[DefaultShuttle] From[Person]: "message"
          |(Regular Message) To[DefaultShuttle] From[Person]: "message"
          |(Regular Message) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle is changing drivers.
          |(DefaultShuttle): Shuttle has switched driver.
          |(DefaultShuttle): Shuttle has departed towards Station(B).
          |""".stripMargin
      )
    }
  }

  """Simulation #2:
     - tripDuration: 0.5 seconds. (500 millis)
     - inverseTripDuration: 1 second. (1000 millis)
     - waitDuration: 2 seconds. (2000 millis)
     - shiftDuration: 4.5 seconds. (4500 millis)
     - shiftChangeDuration: 1 second. (1000 millis)
     - messageInterval: 0.8 seconds. (800 millis)
     - probUrgent: 100% (1)
  """ should{
    "print out a specific order of notifications after 7 seconds" in {
      val stream = new ByteArrayOutputStream
      Console.withOut(stream) {
        new ShuttleSystem(500, 1000, 2000, 4500, 1000, 800, 1).start
        Thread.sleep(7000)
      }
      assert(stream.toString ==
        """(DefaultShuttle): Shuttle has departed towards Station(B).
          |(DefaultShuttle): Shuttle has arrived in Station(B).
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle has departed towards Station(A).
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle has arrived in Station(A).
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle driver's shift ended and will switch driver in the next stop.
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle is changing drivers.
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle has switched driver.
          |(DefaultShuttle): Shuttle has departed towards Station(B).
          |""".stripMargin
      )
    }
  }

  """Simulation #3:
     - tripDuration: 1.8 seconds. (1800 millis)
     - inverseTripDuration: 1.5 seconds. (1500 millis)
     - waitDuration: 3 seconds. (3000 millis)
     - shiftDuration: 8 seconds. (4500 millis)
     - shiftChangeDuration: 1 second. (1000 millis)
     - messageInterval: 1.4 seconds. (800 millis)
     - probUrgent: 50% (0.5)
  """ should{
    "print out a specific order of notifications after 2 seconds" in {
      val stream = new ByteArrayOutputStream
      Console.withOut(stream) {
        new ShuttleSystem(1800, 1500, 3000, 8000, 1000, 1400, 0.5).start
        Thread.sleep(2000)
      }
      assert(stream.toString == //if message was urgent
        """(DefaultShuttle): Shuttle has departed towards Station(B).
          |(URGENT MESSAGE) To[DefaultShuttle] From[Person]: "message"
          |(DefaultShuttle): Shuttle has arrived in Station(B).
          |""".stripMargin ||
        stream.toString == //if message was regular
          """(DefaultShuttle): Shuttle has departed towards Station(B).
            |(DefaultShuttle): Shuttle has arrived in Station(B).
            |(Regular Message) To[DefaultShuttle] From[Person]: "message"
            |""".stripMargin
      )
    }
  }
}
