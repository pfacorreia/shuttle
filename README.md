#Shuttle
The EWR AirTrain shuttles between terminals A & B. The trip takes 9 minutes from Terminal A to Terminal B, but the return journey takes 11 minutes. The train waits 7.5 minutes between each trip except when there is a shift change for the driver, and an additional 5.5 minutes are added to allow for the drivers to swap places. Each driver is limited to an 8 hour shift.
While the train is in transit, if a message is sent to the driver, the sender receives "Driver In Transit. Please try again later" as an automatic response. Once a driver reaches a station, all pending messages are delivered to them.
For a fee, a sender can reply to the auto response with the message "Urgent" and have the driver receive the message immediately. Write a program, preferably in Scala, that simulates a shuttle running for 24 hours with each driver receiving 5 random messages per hour, 2 of which are urgent. Design your solution so it can be easily extended to add additional stations to the route.
To submit your solution, fork this project and send a pull request with your code.

#Notable Changes (version 2)

- Made it so that the program can be started using the "sbt run" command instead of using the main method directly.
- Made the shuttle into an actor itself that handles all the different actions, instead of having 5 different actors for different actions.
- Made functional and unit tests for the program.
- Cleaned up code and added comments for all the objects, classes, methods and values.

#Instructions (version 2)

To run the program, you can now use the "sbt run" command along with the arguments for the shuttle system:

`sbt "run [tripDuration] [tripInverseDuration] [waitDuration] [shiftDuration] [shiftChangeDuration] [messageInterval] [probUrgent]"` (note that the quotation marks are necessary)

or alternatively you can first run the `sbt` command and then you can do:

`run [tripDuration] [tripInverseDuration] [waitDuration] [shiftDuration] [shiftChangeDuration] [messageInterval] [probUrgent]` (no need for quotation marks this way)


[tripDuration] - time the shuttle takes to go from the initial station to the final station (in milliseconds)
[tripInverseDuration] - time the shuttle takes to go from the final station to the initial station (in milliseconds)
[waitDuration] - time the shuttle takes when waiting at a station. (in milliseconds)
[shiftDuration] - duration of the shift for each driver. (in milliseconds)
[shiftChangeDuration] - time it takes for the shuttle to switch drivers after a shift ending. (in milliseconds)
[messageInterval] - interval between each message sent to the driver (in milliseconds)
[probUrgent] - probability of a message to be urgent. (Double between 0 and 1, where 0 means all messages are regular ones, and 1 means all are urgent)


To run the specific scenario asked in the exercise, just type in this command in the terminal:

`sbt "run 540000 660000 450000 28800000 330000 720000 0.4"`

as an alternative you can also run the following command which has downscaled arguments to facilitate testing:

`sbt "run 5400 6600 4500 288000 3300 7200 0.4"`